// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import session from "../../lib/session";

export default async function handler(req, res) {
  console.log("auth::auth " + JSON.stringify(req.session));
  console.log("cookie::cookie " + JSON.stringify(req.cookie));
  if (!req.session) await session(req, res);

  req.session.user = "gowtham";
  console.log("sessionsession " + JSON.stringify(req.session));
  res.status(200).json({ name: "John Doe" });
}
