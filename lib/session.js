import nextSession from "next-session";
import { expressSession, promisifyStore } from "next-session/lib/compat";
import RedisStoreFactory from "connect-redis";
import Redis from "ioredis";

const RedisStore = RedisStoreFactory(expressSession);

const getSession = nextSession({
  name: "session-app",
  store: promisifyStore(
    new RedisStore({
      client: new Redis(),
    })
  ),
  cookie: {
    httpOnly: true,
    secure: process.env.NODE_ENV === "production",
    maxAge: 2 * 7 * 24 * 60 * 60, // 2 weeks,
    path: "/",
    sameSite: "strict",
  },
  touchAfter: 1 * 7 * 24 * 60 * 60,
  secret: "gowtham-app",
  resave: false,
  saveUninitialized: true,
});

export default async function session(req, res) {
  await getSession(req, res);
  console.log("SESSION DATA " + JSON.stringify(req.session));
}
