import { NextResponse } from "next/server";

const aboutus = async (request) => {
  console.log("middleware:inside " + request.nextUrl.pathname);
  console.log("middleware:url " + new URL("/", request.url));
  await fetch(new URL("/api/auth", request.url), {
    method: "GET",
  });
  return NextResponse.redirect(new URL("/", request.url));
};

export const middlewares = {
  aboutus,
};
